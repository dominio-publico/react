import {BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Favorito from './componentes/Favorito';
import Global from './componentes/Global';
import Personajes from './componentes/Personajes'
import Error404 from './componentes/Error404'



function App() {
  return (
    <>
      <navBar/>
      <BrowserRouter>
        <Routes>
          <Route path = "/" element = {<Global/>}></Route>
          <Route path = "/personajes" element = {<Personajes/>}></Route>
          <Route path = "/personajes/favorito" element = {<Favorito/>}></Route>
          <Route path = "*" element = {<Error404/>}></Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
