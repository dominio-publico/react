import BarraNav from './BarraNav'
import Footer from './Footer'
import './Styles.css'

export default function Global() {
    return (
        <>
            <BarraNav/>
            <div id = "content">
                <img id = "imagen" src="https://i.postimg.cc/CMD0kcHk/rick-n-morty-wallpaper.jpg"/>
            </div>
            <Footer info = "@ Álvaro Román de la Flor SL"/>
        </>
    )
}