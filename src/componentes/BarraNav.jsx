import { Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css'
import './Styles.css'

export default function BarraNav(){
    let buttons = (
        <div>
            <Button id = "boton" 
            href="http://localhost:3001/" className = "mt-3 mb-2" variant="warning">INICIO</Button>
            <Button id = "boton" 
            href="http://localhost:3001/personajes" className = "mt-3 mb-2" variant="primary">PERSONAJES</Button>
            <Button id = "boton"  
            href="http://localhost:3001/personajes/favorito" className = "mt-3 mb-2" variant="warning">FAVORITO</Button>
        </div>
    )
    return(
        <header>
            <nav>
                <ul>{buttons}</ul>
            </nav>
        </header>
    )
}
