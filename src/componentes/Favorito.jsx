import BarraNav from './BarraNav'
import Footer from './Footer'
import './Styles.css'
import axios from "axios"; 
import React from "react"; 

const baseURL = "http://localhost:3000/personajes/favorito"; 

export default function Favorito() {
    const [personajes, setPersonajes] = React.useState([]); 
    React.useEffect(() => { 
        axios.get(baseURL).then((response) => { 
        setPersonajes(response.data.personaje); }); 
        
    }, []); 

    return (
        <>  
            <BarraNav/>
            
            <div id = "content2">
                <div id = "carta">
                    <img id = "imagen2" src={personajes.image}/>
                    <p><br/><b>Nombre:</b> {personajes.name}</p>
                    <p><b>Especie:</b> {personajes.species}</p>
                </div>
            </div> 
        
            <Footer info = "@ Álvaro Román de la Flor SL"/>
        </>
    )
}