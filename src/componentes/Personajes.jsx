import BarraNav from './BarraNav'
import Footer from './Footer'
import './Styles.css'
import axios from "axios"; 
import React from "react"; 
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import {useHistory} from 'react-router-dom'

const baseURL = "http://localhost:3000/personajes/verPersonajes"; 

export default function Personajes() {
    const navigate = useNavigate()
    const [personajes, setPersonajes] = React.useState([]); 
    React.useEffect(() => { 
        axios.get(baseURL).then((response) => { 
        setPersonajes(response.data.personaje); }); 
        
    }, []); 

    let listar = personajes.map(dato => (
        <div id = "carta1">
            <img id="imagen2"src={dato.image}/>
            <p><br/><b>Nombre: </b> {dato.name}</p>
            <p><b>Especie: </b>{dato.species}</p>
            <Button id = "boton1" href={"http://localhost:3000/personajes/borrarPersonaje/" + dato.id} onClick={() => {
                navigate('/')
            }}variant="warning">BORRAR</Button>
        </div>
    ))

    return (
        <>  
            <BarraNav/>
            
            <div id="content3"> 
                <div id="contenido">
                    {listar}
                </div>
            </div> 
        
            <Footer info = "@ Álvaro Román de la Flor SL"/>
        </>
    )
}