import './Styles.css'

export default function Footer(props){
    return(
        <footer id="pie">{props.info}</footer>
    )
}
